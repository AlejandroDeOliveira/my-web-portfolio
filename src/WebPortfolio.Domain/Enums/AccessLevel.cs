﻿namespace WebPortfolio.Domain.Enums
{
    public enum  AccessLevel
    {
        Guest = 0,        // 0 - Can view public content
        RegisteredUser = 1, // 1 - Can comment and interact with basic features
        Contributor = 2,  // 2 - Can create and manage own content
        Moderator = 3,    // 3 - Can manage other users' content
        Administrator = 4 // 4 - Full access to manage the application
    }
}
