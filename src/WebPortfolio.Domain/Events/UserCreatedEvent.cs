﻿using WebPortfolio.Domain.Common;
using WebPortfolio.Domain.Entities;

namespace WebPortfolio.Domain.Events
{
    public class UserCreatedEvent : BaseEvent
    {
        public UserCreatedEvent(PortfolioUser user)
        {
            User = user;
        }

        public PortfolioUser User { get; }
    }
}
