﻿namespace WebPortfolio.ArchitecturalTests
{
    public class LayeringTests
    {
        private static readonly Assembly ApplicationAssembly = Assembly.Load("WebPortfolio.Application");
        private static readonly Assembly BlazorServerAssembly = Assembly.Load("WebPortfolio.BlazorServer");
        private static readonly Assembly DomainAssembly = Assembly.Load("WebPortfolio.Domain");
        private static readonly Assembly InfrastructureAssembly = Assembly.Load("WebPortfolio.Infrastructure");

        [Fact]
        public void BlazorServer_ShouldNotReference_Application()
        {
            var references = BlazorServerAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Application");
        }

        [Fact]
        public void BlazorServer_ShouldNotReference_Domain()
        {
            var references = BlazorServerAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Domain");
        }

        [Fact]
        public void BlazorServer_ShouldNotReference_Infrastructure()
        {
            var references = BlazorServerAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Infrastructure");
        }

        [Fact]
        public void Application_ShouldNotReference_BlazorServer()
        {
            var references = ApplicationAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.BlazorServer");
        }

        [Fact]
        public void Application_ShouldNotReference_Domain()
        {
            var references = ApplicationAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Domain");
        }

        [Fact]
        public void Application_ShouldNotReference_Infrastructure()
        {
            var references = ApplicationAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Infrastructure");
        }

        [Fact]
        public void Domain_ShouldNotReference_BlazorServer()
        {
            var references = DomainAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.BlazorServer");
        }

        [Fact]
        public void Domain_ShouldNotReference_Application()
        {
            var references = DomainAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Application");
        }

        [Fact]
        public void Domain_ShouldNotReference_Infrastructure()
        {
            var references = DomainAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Infrastructure");
        }

        [Fact]
        public void Infrastructure_ShouldNotReference_BlazorServer()
        {
            var references = InfrastructureAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.BlazorServer");
        }

        [Fact]
        public void Infrastructure_ShouldNotReference_Application()
        {
            var references = InfrastructureAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Application");
        }

        [Fact]
        public void Infrastructure_ShouldNotReference_Domain()
        {
            var references = InfrastructureAssembly.GetReferencedAssemblies().Select(a => a.Name).ToList();
            references.Should().NotContain("WebPortfolio.Domain");
        }
    }
}
